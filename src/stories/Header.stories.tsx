import React from 'react'

import { Story } from '@storybook/react'

import Header, { HeaderProps } from '../components/Header'

export default {
    title: 'Header',
    component: Header,
}

const Template: Story<HeaderProps> = (args) => <Header {...args} />

export const DefaultHeader = Template.bind({})
DefaultHeader.args = {}
