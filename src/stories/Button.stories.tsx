import React from 'react'

import { Story } from '@storybook/react'

import { Button, ButtonProps } from 'theme-ui'
import { text } from '@storybook/addon-knobs'

export default {
    title: 'Button',
    component: Button,
    argTypes: { onClick: { action: 'onClick' } },
}

const Template: Story<ButtonProps> = (args) => (
    <Button {...args}>{text('label', 'Button')}</Button>
)

export const PrimaryButton = Template.bind({})
PrimaryButton.args = {
    variant: 'primary',
}

export const SecondaryButton = Template.bind({})
SecondaryButton.args = {
    variant: 'secondary',
}
