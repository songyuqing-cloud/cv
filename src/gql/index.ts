import { gql } from '@apollo/client'

export enum MessageStatus {
    SENT = '0',
    SEEN = '1',
    REVOKE = '2',
    REMOVE = '3',
}

export const MESSAGES = gql`
    query GetMessages($conversationId: String!, $offset: Float, $limit: Float) {
        messages(
            conversationId: $conversationId
            offset: $offset
            limit: $limit
        ) {
            _id
            content
            sender {
                _id
                firstName
                lastName
            }
            createdAt
            status
            message {
                _id
                content
                sender {
                    _id
                    firstName
                    lastName
                }
                createdAt
                status
            }
        }
    }
`

export const CONVERSATION = gql`
    query GetConversation($conversationId: String!) {
        conversation(conversationId: $conversationId) {
            participants {
                _id
            }
        }
    }
`

export const NEW_MESSAGE = gql`
    subscription OnNewMessage($conversation: String!) {
        newMessage(conversation: $conversation) {
            _id
            content
            sender {
                _id
                firstName
                lastName
            }
            createdAt
            status
            message {
                _id
                content
                sender {
                    _id
                    firstName
                    lastName
                }
                createdAt
                status
            }
        }
    }
`

export const CREATE_MESSAGE = gql`
    mutation MessageMutation(
        $_id: String
        $conversation: ID!
        $content: String!
        $message: ID
    ) {
        createMessage(
            input: {
                _id: $_id
                conversation: $conversation
                content: $content
                message: $message
            }
        ) {
            content
        }
    }
`

export const LAST_SEEN = gql`
    mutation LastSeenMutation($conversationId: String!) {
        lastSeen(conversationId: $conversationId) {
            currentUser {
                _id
            }
        }
    }
`

export const TYPING = gql`
    mutation TypingMutation($conversationId: String!, $typing: Boolean!) {
        typing(conversationId: $conversationId, typing: $typing) {
            user {
                _id
            }
            typing
        }
    }
`

export const NEW_TYPING = gql`
    subscription OnNewTyping($conversation: String!) {
        newTyping(conversation: $conversation) {
            user {
                _id
            }
            typing
        }
    }
`

export const NEW_LAST_SEEN = gql`
    subscription OnNewLastSeen($conversation: String!) {
        newLastSeen(conversation: $conversation) {
            currentUser {
                _id
            }
            conversation {
                _id
            }
        }
    }
`

export const SIGN_UP = gql`
    mutation SignUpMutation(
        $email: String!
        $password: String!
        $firstName: String!
        $lastName: String!
        $phoneNumber: String!
    ) {
        signUp(
            input: {
                email: $email
                password: $password
                firstName: $firstName
                lastName: $lastName
                phoneNumber: $phoneNumber
            }
        )
    }
`

export const LOGIN = gql`
    mutation LoginMutation($email: String!, $password: String!) {
        signIn(input: { email: $email, password: $password })
    }
`
