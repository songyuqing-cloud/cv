import React, { FC, memo } from 'react'
import { Flex, Box } from 'theme-ui'
import { keyframes } from '@emotion/react'

const wave = keyframes`
    0%,
    60%,
    100%    {
      transform: initial;
    }
    30% {
      transform: translateY(-5px);
    }
`

const Dot: FC<{ animationDelay: number }> = memo(({ animationDelay }) => (
    <Box
        bg="textSecondary"
        mx="2px"
        sx={{
            display: 'inline-block',
            width: 5,
            height: 5,
            borderRadius: 9999,
            animation: `${wave} 1.3s linear infinite`,
            animationDelay: `${animationDelay}s`,
        }}
    />
))

const Typing: FC = () => {
    return (
        <Flex
            mb="2px"
            py={2}
            px={3}
            mr="auto"
            sx={{
                borderRadius: 2,
                borderStyle: 'solid',
                borderWidth: 1,
                borderColor: 'message',
                overflowWrap: 'break-word',
                height: 36,
                alignItems: 'center',
            }}
        >
            <Dot animationDelay={0} />
            <Dot animationDelay={-1.1} />
            <Dot animationDelay={-0.9} />
        </Flex>
    )
}

export default memo(Typing)
