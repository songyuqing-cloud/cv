import { Flex, Box, Switch, Text, useColorMode } from 'theme-ui'
import React, { FC, memo, useCallback, useRef, useState } from 'react'
import useOnClickOutside from 'use-onclickoutside'
import { useQuery } from '@apollo/client'
import { Popover } from 'react-tiny-popover'
import Link from 'next/link'

import { useRouter } from 'next/router'
import Menu from './Menu'
import { CurrentUser, CURRENT_USER } from '../pages'
import useLocalStorage from '../hooks/local-storage'

const items = [
    { id: 1, label: 'About', value: '/about' },
    { id: 2, label: 'Resume', value: '/resume' },
    { id: 3, label: 'Contact', value: '/contact' },
]

const Header: FC = () => {
    const [open, setOpen] = useState<boolean>(false)
    const ref = useRef<HTMLDivElement>(null)
    const router = useRouter()
    const { pathname = '' } = router
    const [colorMode, setColorMode] = useColorMode()
    const [showDetail, setShowDetail] = useState<boolean>(false)
    const closeMenu = useCallback(() => {
        setOpen(false)
    }, [setOpen])
    const { setValue: setToken } = useLocalStorage('auth-token', '')
    const { data, client } = useQuery<{
        currentUser: CurrentUser
    }>(CURRENT_USER, {
        errorPolicy: 'all',
        fetchPolicy: 'network-only',
    })
    const checkActive = useCallback(
        (value: string) => {
            if (value === 'about' && pathname === '/') {
                return true
            }
            return new RegExp(value).test(pathname)
        },
        [pathname]
    )

    const logout = useCallback(() => {
        setToken('')
        client.clearStore().then(() => {
            client.resetStore()
            router.push('/login')
        })
    }, [client, setToken, router])
    useOnClickOutside(ref, closeMenu)
    return (
        <Flex
            ref={ref}
            bg="background"
            sx={{
                position: ['sticky', 'relative'],
                boxShadow: ['large', 'none'],
                top: 0,
                zIndex: 9999,
            }}
        >
            <Flex
                px={[20, 40]}
                py={[10, 20]}
                bg="background"
                sx={{
                    position: 'relative',
                    flex: 1,
                    alignItems: 'center',
                }}
            >
                <Link href="/" passHref>
                    <Box
                        as="a"
                        mr="auto"
                        sx={{
                            borderRadius: '100%',
                            bg: 'primary',
                            width: [33, 44],
                            height: [33, 44],
                            textAlign: 'center',
                            lineHeight: ['32px', '44px'],
                            fontWeight: 'bold',
                            color: 'white',
                            cursor: 'pointer',
                            fontSize: [18, 28],
                            ':hover': {
                                boxShadow: 'small',
                            },
                        }}
                    >
                        X
                    </Box>
                </Link>
                <Flex
                    sx={{
                        position: ['fixed', 'relative'],
                        bg: 'background',
                        top: [53, 'unset'],
                        left: ['auto', null],
                        right: [open ? 0 : '-350px', 'unset'],
                        bottom: [0, null],
                        width: ['100%', 'unset'],
                        maxWidth: [350, 'none'],
                        transition: 'right 0.33s, opacity 0.33s ease-in-out',
                        opacity: [open ? 1 : 0, 1],
                        flexDirection: ['column', 'row'],
                        alignItems: [null, 'center'],
                        zIndex: [-1, 'unset'],
                        boxShadow: ['large', 'none'],
                    }}
                >
                    {items.map((item) => (
                        <Link key={item.id} href={item.value} passHref>
                            <Flex
                                as="a"
                                mx={[30, 15]}
                                sx={{
                                    alignItems: 'center',
                                    cursor: 'pointer',
                                    opacity: checkActive(item.value) ? 1 : 0.55,
                                    transition: 'opacity 0.33s ease-in-out',
                                    ':hover': {
                                        opacity: 1,
                                    },
                                }}
                                onClick={closeMenu}
                            >
                                <Text
                                    color="text"
                                    sx={{
                                        fontSize: 2,
                                        lineHeight: '50px',
                                    }}
                                >
                                    {item.label}
                                </Text>
                            </Flex>
                        </Link>
                    ))}
                    {data?.currentUser ? (
                        <>
                            <Link href="/chat" passHref>
                                <Flex
                                    as="a"
                                    mx={[30, 15]}
                                    sx={{
                                        alignItems: 'center',
                                        cursor: 'pointer',
                                        opacity: checkActive('/chat')
                                            ? 1
                                            : 0.55,
                                        transition: 'opacity 0.33s ease-in-out',
                                        ':hover': {
                                            opacity: 1,
                                        },
                                    }}
                                    onClick={closeMenu}
                                >
                                    <Text
                                        color="text"
                                        sx={{
                                            fontSize: 2,
                                            lineHeight: '50px',
                                        }}
                                    >
                                        Chat
                                    </Text>
                                </Flex>
                            </Link>
                            <Popover
                                onClickOutside={() => setShowDetail(false)}
                                isOpen={showDetail}
                                positions={['bottom']}
                                containerStyle={{
                                    zIndex: '9999',
                                }}
                                content={
                                    <Flex
                                        bg="background"
                                        p={15}
                                        sx={{
                                            boxShadow: 'small',
                                            minWidth: [200, 100],
                                            flexDirection: 'column',
                                            justifyContent: 'center',
                                        }}
                                    >
                                        <Flex
                                            onClick={() => setShowDetail(false)}
                                            sx={{
                                                flex: 1,
                                                cursor: 'pointer',
                                                opacity: 0.55,
                                                transition:
                                                    'opacity 0.33s ease-in-out',
                                                ':hover': {
                                                    opacity: 1,
                                                },
                                            }}
                                        >
                                            <Text
                                                color="text"
                                                sx={{
                                                    fontSize: 2,
                                                    lineHeight: '30px',
                                                }}
                                            >
                                                Profile
                                            </Text>
                                        </Flex>
                                        <Flex
                                            onClick={() => {
                                                logout()
                                                setShowDetail(false)
                                            }}
                                            sx={{
                                                flex: 1,
                                                cursor: 'pointer',
                                                opacity: 0.55,
                                                transition:
                                                    'opacity 0.33s ease-in-out',
                                                ':hover': {
                                                    opacity: 1,
                                                },
                                            }}
                                        >
                                            <Text
                                                color="text"
                                                sx={{
                                                    fontSize: 2,
                                                    lineHeight: '30px',
                                                }}
                                            >
                                                Logout
                                            </Text>
                                        </Flex>
                                    </Flex>
                                }
                                // place="below"
                                // tipSize={0.01}
                            >
                                <Flex
                                    onClick={() => setShowDetail(!showDetail)}
                                    mx={[30, 15]}
                                    sx={{
                                        alignItems: 'center',
                                        cursor: 'pointer',
                                        opacity: 0.55,
                                        transition: 'opacity 0.33s ease-in-out',
                                        ':hover': {
                                            opacity: 1,
                                        },
                                    }}
                                >
                                    <Text
                                        color="text"
                                        sx={{
                                            fontSize: 2,
                                            lineHeight: '50px',
                                        }}
                                    >
                                        Hi, {data?.currentUser?.firstName}
                                    </Text>
                                </Flex>
                            </Popover>
                        </>
                    ) : (
                        <Link href="/login" passHref>
                            <Flex
                                mx={[30, 15]}
                                as="a"
                                sx={{
                                    alignItems: 'center',
                                    cursor: 'pointer',
                                    opacity: checkActive('login') ? 1 : 0.55,
                                    transition: 'opacity 0.33s ease-in-out',
                                    ':hover': {
                                        opacity: 1,
                                    },
                                }}
                            >
                                <Text
                                    color="text"
                                    sx={{
                                        fontSize: 2,
                                        lineHeight: '50px',
                                    }}
                                >
                                    Login
                                </Text>
                            </Flex>
                        </Link>
                    )}
                    <Flex
                        mx={[30, 15]}
                        sx={{
                            height: 50,
                            alignItems: 'center',
                            cursor: 'pointer',
                            opacity: 0.55,
                            transition: 'opacity 0.33s ease-in-out',
                            ':hover': {
                                opacity: 1,
                            },
                        }}
                    >
                        <Text
                            mr={3}
                            color="text"
                            sx={{
                                fontSize: 2,
                                flexShrink: 0,
                            }}
                        >
                            Dark mode
                        </Text>
                        <Switch
                            onChange={() =>
                                setColorMode(
                                    colorMode === 'default' ? 'dark' : 'default'
                                )
                            }
                            checked={colorMode === 'dark'}
                        />
                    </Flex>
                </Flex>
                <Box sx={{ display: [null, 'none'] }}>
                    <Menu open={open} onChange={() => setOpen(!open)} />
                </Box>
            </Flex>
        </Flex>
    )
}

export default memo(Header)
