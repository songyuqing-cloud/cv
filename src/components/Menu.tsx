import { Box, BoxProps } from '@theme-ui/components'
import React, { FC, memo } from 'react'

export interface MenuProps extends BoxProps {
    open: boolean
    onChange?: () => void
}

const MenuItem: FC = () => (
    <Box
        bg="text"
        sx={{
            position: 'absolute',
            height: 3,
            width: '100%',
            borderRadius: 9,
            opacity: 1,
            left: 0,
            transform: 'rotate(0deg)',
            transition: '0.25s ease-in-out',
        }}
    />
)

const Menu: FC<MenuProps> = ({ open, onChange, ...boxProps }) => {
    return (
        <Box
            onClick={onChange}
            sx={{
                position: 'relative',
                width: 24,
                height: 24,
                transform: 'rotate(0deg)',
                transition: '0.5s ease-in-out',
                cursor: 'pointer',
                'div:nth-of-type(1)': {
                    top: '3px',
                    transformOrigin: 'left center',
                    ...(open && { transform: 'rotate(45deg)' }),
                },
                'div:nth-of-type(2)': {
                    top: '10px',
                    transformOrigin: 'left center',
                    ...(open && { width: '0%', opacity: 0 }),
                },
                'div:nth-of-type(3)': {
                    top: '17px',
                    transformOrigin: 'left center',
                    ...(open && {
                        transform: 'rotate(-45deg)',
                        top: 20,
                    }),
                },
            }}
            {...boxProps}
        >
            <MenuItem />
            <MenuItem />
            <MenuItem />
        </Box>
    )
}

export default memo(Menu)
