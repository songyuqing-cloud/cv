import React, { FC, memo, useMemo } from 'react'
import { Button, Flex, Text, useColorMode } from 'theme-ui'
import ReactEmoji from 'react-emoji'
import Linkify from 'react-linkify'
import dayjs from 'dayjs'
import ReplyIcon from '../assets/icons/reply.svg'

interface Message {
    _id: string
    content: string
    createdAt: Date
    sender: { _id: string; firstName: string; lastName: string }
    message?: Message
    status: 'SENT' | 'SEEN' | 'REVOKE' | 'REMOVE'
}

interface MessageProps {
    partner?: boolean
    message: Message
    nextMessage: Message
    prevMessage: Message
    userId: string
    onReply: () => void
}

const Message: FC<MessageProps> = ({
    message,
    partner,
    nextMessage,
    prevMessage,
    userId,
    onReply,
}) => {
    const time = useMemo(() => {
        if (dayjs().isSame(dayjs(message.createdAt), 'year')) {
            if (dayjs().isSame(dayjs(message.createdAt), 'month')) {
                if (dayjs().isSame(dayjs(message.createdAt), 'week')) {
                    if (dayjs().isSame(dayjs(message.createdAt), 'day')) {
                        return dayjs(message.createdAt).format('HH:ss')
                    }
                    return dayjs(message.createdAt).format('dd HH:ss')
                }
                return dayjs(message.createdAt).format('DD HH:ss')
            }
            return dayjs(message.createdAt).format('DD/MM HH:ss')
        }
        return dayjs(message.createdAt).format('DD/MM/YYYY HH:ss')
    }, [message.createdAt])
    const isNew = useMemo(
        () =>
            new Date(message?.createdAt).getTime() -
                new Date(prevMessage?.createdAt).getTime() >
            300000,
        [message, prevMessage]
    )
    const isOld = useMemo(
        () =>
            new Date(nextMessage?.createdAt).getTime() -
                new Date(message?.createdAt).getTime() >
            300000,
        [message, nextMessage]
    )
    const isFirst = useMemo(
        () => message?.sender?._id !== prevMessage?.sender._id,
        [message, prevMessage]
    )
    const isLast = useMemo(
        () => message?.sender?._id !== nextMessage?.sender._id,
        [message, nextMessage]
    )
    const detail = useMemo(() => {
        if (message?.message) {
            if (message.message.sender._id === userId) {
                if (message.sender._id === userId) {
                    return 'Bạn đã trả lời chính bạn'
                }
                return `${message.message.sender.firstName} đã trả lời bạn`
            }
            if (message.sender._id === userId) {
                return `Bạn đã trả lời ${message.message.sender.firstName}`
            }
            return `${message.message.sender.firstName} đã trả lời ${message.message.sender.firstName}`
        }
        return ''
    }, [message, userId])
    const [theme] = useColorMode()
    return (
        <Flex sx={{ flexDirection: 'column' }}>
            {isNew && (
                <Flex sx={{ justifyContent: 'center' }}>
                    <Text
                        color="textSecondary"
                        sx={{
                            fontSize: 0,
                            textAlign: 'center',
                        }}
                    >
                        {time}
                    </Text>
                </Flex>
            )}
            <Flex
                {...(partner
                    ? { mr: 'auto' }
                    : {
                          ml: 'auto',
                      })}
                sx={{
                    maxWidth: '80%',
                    flexDirection: 'column',
                }}
            >
                {detail && (
                    <>
                        <Flex
                            sx={{
                                alignItems: 'center',
                                justifyContent: partner
                                    ? 'flex-start'
                                    : 'flex-end',
                            }}
                        >
                            <ReplyIcon width={10} height={10} />
                            <Text
                                ml={1}
                                color="textSecondary"
                                sx={{
                                    fontSize: 0,
                                }}
                            >
                                {detail}
                            </Text>
                        </Flex>
                        <Flex
                            mb={-3}
                            {...(partner
                                ? { bg: 'background' }
                                : {
                                      bg: 'message',
                                  })}
                            pt={2}
                            pb="18px"
                            px={3}
                            sx={{
                                opacity: 0.6,
                                borderRadius: 1,
                                borderStyle: 'solid',
                                borderWidth: 1,
                                borderColor: 'message',
                                overflowWrap: 'break-word',
                                cursor: 'pointer',
                            }}
                        >
                            <Text
                                color={
                                    theme === 'dark' && !partner
                                        ? 'black'
                                        : 'text'
                                }
                                sx={{ fontSize: 0 }}
                                title={time}
                            >
                                <Linkify target="_blank">
                                    {ReactEmoji.emojify(
                                        message.message.content
                                    )}
                                </Linkify>
                            </Text>
                        </Flex>
                    </>
                )}
                <Flex
                    sx={{
                        ...{
                            ':hover': {
                                button: {
                                    opacity: 1,
                                },
                            },
                        },
                        ...(!partner && { flexDirection: 'row-reverse' }),
                    }}
                >
                    <Flex
                        mb="2px"
                        {...(partner
                            ? { bg: 'background' }
                            : {
                                  bg: 'message',
                              })}
                        py={2}
                        px={3}
                        sx={{
                            ...{
                                zIndex: 1,
                                borderRadius: 0,
                                borderStyle: 'solid',
                                borderWidth: 1,
                                borderColor: 'message',
                                overflowWrap: 'break-word',
                            },
                            ...(partner
                                ? {
                                      ...{
                                          borderTopRightRadius: 1,
                                          borderBottomRightRadius: 1,
                                      },
                                      ...((isFirst || isNew) && {
                                          borderTopLeftRadius: 1,
                                      }),
                                      ...((isOld || isLast) && {
                                          borderBottomLeftRadius: 1,
                                      }),
                                  }
                                : {
                                      ...{
                                          borderTopLeftRadius: 1,
                                          borderBottomLeftRadius: 1,
                                      },
                                      ...((isFirst || isNew) && {
                                          borderTopRightRadius: 1,
                                      }),
                                      ...((isOld || isLast) && {
                                          borderBottomRightRadius: 1,
                                      }),
                                  }),
                        }}
                    >
                        <Text
                            color={
                                theme === 'dark' && !partner ? 'black' : 'text'
                            }
                            sx={{ fontSize: 2 }}
                            title={time}
                        >
                            <Linkify target="_blank">
                                {ReactEmoji.emojify(message.content)}
                            </Linkify>
                        </Text>
                    </Flex>
                    <Button
                        p={2}
                        variant="transparent"
                        sx={{
                            opacity: 0,
                            zIndex: 1,
                            transition: '0.3s',
                        }}
                        onClick={onReply}
                    >
                        <ReplyIcon width={20} height={20} />
                    </Button>
                </Flex>
                {message.status === 'SEEN' && !partner && !nextMessage && (
                    <Text
                        px={2}
                        color="textSecondary"
                        sx={{ fontSize: 0, textAlign: 'right' }}
                    >
                        Đã xem
                    </Text>
                )}
            </Flex>
        </Flex>
    )
}

export default memo(Message)
