import {
    useLazyQuery,
    useMutation,
    useQuery,
    useSubscription,
} from '@apollo/client'
import { useRouter } from 'next/router'
import React, {
    FC,
    useCallback,
    useEffect,
    useRef,
    useState,
    useMemo,
    memo,
} from 'react'
import Head from 'next/head'
import { Box, Button, Flex, Input, Text, Spinner, Close } from 'theme-ui'
import { keyframes } from '@emotion/react'
import InfiniteScroll from 'react-infinite-scroll-component'
import ReactEmoji from 'react-emoji'
import Linkify from 'react-linkify'
import { v4 as uuidv4 } from 'uuid'
import { SubmitHandler, useForm } from 'react-hook-form'
import { CurrentUser, CURRENT_USER } from '..'
import Typing from '../../components/Typing'
import {
    CREATE_MESSAGE,
    MESSAGES,
    NEW_MESSAGE,
    NEW_TYPING,
    TYPING,
    LAST_SEEN,
    NEW_LAST_SEEN,
} from '../../gql'
import Message from '../../components/Message'

const LIMIT_MESSAGE = 20

const jump = keyframes`
        0%   { transform: translateY(0); }
        50%  { transform: translateY(-3px); }
        100% { transform: translateY(0); }
`
interface IFormInput {
    content: string
}

interface IMessage {
    _id: string
    content: string
    sender: CurrentUser
}

const HaveNewMessage: FC<{ onClick: () => void }> = memo(({ onClick }) => (
    <Flex
        px={2}
        bg="background"
        sx={{
            zIndex: 2,
            height: 30,
            boxShadow: 'small',
            position: 'absolute',
            bottom: 42,
            right: 0,
            borderRadius: 1,
            cursor: 'pointer',
            animation: `${jump} 1s ease infinite`,
            alignItems: 'center',
        }}
    >
        <Text
            onClick={onClick}
            color="text"
            sx={{
                fontSize: 1,
            }}
        >
            Có tin nhắn mới
        </Text>
    </Flex>
))

const ConversationPage: FC = () => {
    const router = useRouter()
    const { conversationId = '' } = router.query
    const [hasMore, setHasMore] = useState<boolean>(true)
    const scrollRef = useRef<HTMLDivElement>(null)
    const messagesEndRef = useRef<HTMLDivElement>(null)
    const [newMessage, setNewMessage] = useState<IMessage | null>(null)
    const [replyMessage, setReplyMessage] = useState<IMessage | null>(null)
    const [showScrollToBottom, setShowScrollToBottom] = useState<boolean>(false)
    const [
        getMessages,
        { subscribeToMore, data: dataMessages, fetchMore, updateQuery },
    ] = useLazyQuery(MESSAGES, {
        errorPolicy: 'all',
        variables: {
            conversationId,
            offset: 0,
            limit: LIMIT_MESSAGE,
        },
    })
    const { data: newTyping } = useSubscription(NEW_TYPING, {
        variables: { conversation: conversationId },
    })
    const { data: newLastSeen } = useSubscription(NEW_LAST_SEEN, {
        variables: { conversation: conversationId },
    })
    const [onTyping] = useMutation(TYPING, {
        errorPolicy: 'all',
    })
    const [onLastSeen] = useMutation(LAST_SEEN, {
        errorPolicy: 'all',
    })
    const [createMessage] = useMutation(CREATE_MESSAGE, {
        errorPolicy: 'all',
    })
    const { data: dataUser } = useQuery<{
        currentUser: CurrentUser
    }>(CURRENT_USER, {
        errorPolicy: 'all',
        fetchPolicy: 'network-only',
    })
    const userId = useMemo(() => dataUser?.currentUser?._id, [
        dataUser?.currentUser?._id,
    ])
    const {
        register,
        handleSubmit,
        reset,
        setFocus,
        formState: { isDirty },
    } = useForm<IFormInput>({ defaultValues: { content: '' } })
    const fetchMoreMessage = useCallback(() => {
        if (conversationId) {
            fetchMore({
                variables: {
                    conversationId,
                    offset: Number((dataMessages?.messages ?? []).length) ?? 0,
                    limit: LIMIT_MESSAGE,
                },
            }).then((fetchMoreResult) => {
                const newEntries = fetchMoreResult?.data?.messages
                if ((newEntries ?? []).length < LIMIT_MESSAGE) {
                    setHasMore(false)
                }
            })
        }
    }, [fetchMore, conversationId, dataMessages?.messages])
    const subscribeToNewMessage = useCallback(() => {
        if (subscribeToMore && userId && conversationId) {
            subscribeToMore({
                document: NEW_MESSAGE,
                variables: { conversation: conversationId },
                updateQuery: (prev, { subscriptionData }) => {
                    if (!subscriptionData.data) return prev
                    const newFeedItem = subscriptionData.data.newMessage
                    if (
                        prev.messages.find(
                            (item) => item._id === newFeedItem._id
                        )
                    ) {
                        const newData = prev.messages.map((item) =>
                            item._id === newFeedItem._id ? newFeedItem : item
                        )
                        return {
                            messages: newData,
                        }
                    }
                    setNewMessage(newFeedItem)
                    return {
                        ...prev,
                        messages: [newFeedItem, ...prev.messages],
                    }
                },
            })
        }
    }, [subscribeToMore, conversationId, userId])
    const scrollToBottom = useCallback(() => {
        messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' })
        if (newMessage) {
            setNewMessage(null)
        }
    }, [newMessage])
    const onSubmit = useCallback<SubmitHandler<IFormInput>>(
        ({ content }) => {
            if (conversationId && dataUser?.currentUser) {
                const _id = uuidv4()
                const mess = {
                    _id,
                    createdAt: new Date().toISOString(),
                    message: replyMessage || null,
                    content,
                    status: 'SEND',
                    sender: dataUser?.currentUser,
                }
                updateQuery((prev) => {
                    setNewMessage(mess)
                    return {
                        ...prev,
                        messages: [mess, ...prev.messages],
                    }
                })
                createMessage({
                    variables: {
                        ...{ conversation: conversationId, content, _id },
                        ...(replyMessage && { message: replyMessage._id }),
                    },
                })
                reset()
                setReplyMessage(null)
            }
        },
        [
            createMessage,
            conversationId,
            reset,
            replyMessage,
            dataUser?.currentUser,
            updateQuery,
        ]
    )
    const onSeen = useCallback(() => {
        if (conversationId && dataMessages?.messages) {
            const lastMessage = dataMessages.messages.find(
                (item) => item.sender._id !== userId && item.status !== 'SEEN'
            )
            if (lastMessage) {
                onLastSeen({
                    variables: {
                        conversationId,
                    },
                })
            }
        }
    }, [conversationId, onLastSeen, dataMessages?.messages, userId])
    const onScroll = useCallback(() => {
        if (scrollRef.current.scrollTop === 0) {
            setShowScrollToBottom(false)
        }
    }, [])
    useEffect(() => {
        if (newMessage && dataMessages?.messages[0]?._id === newMessage._id) {
            if (
                newMessage.sender._id !== userId &&
                scrollRef.current.scrollTop !== 0
            )
                setShowScrollToBottom(true)
            else scrollToBottom()
        } else if (!showScrollToBottom) setShowScrollToBottom(false)
    }, [
        newMessage,
        userId,
        scrollToBottom,
        dataMessages?.messages,
        showScrollToBottom,
    ])
    useEffect(() => {
        if (conversationId) {
            if (isDirty) {
                onTyping({ variables: { conversationId, typing: true } })
            } else {
                onTyping({ variables: { conversationId, typing: false } })
            }
        }
    }, [isDirty, onTyping, conversationId])
    useEffect(() => subscribeToNewMessage(), [subscribeToNewMessage])
    useEffect(() => {
        if (conversationId) {
            getMessages()
        }
    }, [getMessages, conversationId])
    useEffect(() => {
        setFocus('content')
    }, [setFocus])
    useEffect(() => {
        if (newLastSeen && userId) {
            updateQuery((prev) => {
                const newData = prev.messages.map((item) =>
                    item.sender._id === userId && item.status !== 'SEEN'
                        ? { ...item, status: 'SEEN' }
                        : item
                )
                return {
                    messages: newData,
                }
            })
        }
    }, [newLastSeen, updateQuery, userId])
    return (
        <>
            <Head>{newMessage && <title>Có tin nhắn mới</title>}</Head>
            <Flex
                sx={{
                    position: 'relative',
                    flexDirection: 'column',
                }}
            >
                <Flex
                    ref={scrollRef}
                    id="scrollableDiv"
                    sx={{
                        height: 300,
                        overflow: 'auto',
                        flexDirection: 'column-reverse',
                    }}
                    onScroll={onScroll}
                >
                    <InfiniteScroll
                        dataLength={(dataMessages?.messages ?? []).length}
                        next={fetchMoreMessage}
                        style={{
                            display: 'flex',
                            flexDirection: 'column-reverse',
                        }}
                        inverse
                        hasMore={hasMore}
                        loader={
                            <Flex sx={{ justifyContent: 'center' }}>
                                <Spinner size={20} />
                            </Flex>
                        }
                        scrollableTarget="scrollableDiv"
                        endMessage={
                            <Flex sx={{ justifyContent: 'center' }}>
                                <Text>Bạn đã xem toàn bộ tin nhắn</Text>
                            </Flex>
                        }
                    >
                        <Box ref={messagesEndRef} />
                        {newTyping?.newTyping?.typing && <Typing />}
                        {dataMessages?.messages?.map((message, index) => (
                            <Message
                                key={message._id}
                                partner={userId !== message?.sender?._id}
                                message={message}
                                prevMessage={dataMessages?.messages[index + 1]}
                                nextMessage={dataMessages?.messages[index - 1]}
                                onReply={() => {
                                    setReplyMessage(message)
                                    setFocus('content')
                                }}
                                userId={userId}
                            />
                        ))}
                    </InfiniteScroll>
                </Flex>
                {showScrollToBottom && (
                    <HaveNewMessage onClick={scrollToBottom} />
                )}
                <Flex
                    as="form"
                    onSubmit={handleSubmit(onSubmit)}
                    sx={{
                        borderRadius: 15,
                        borderStyle: 'solid',
                        borderWidth: 1,
                        borderColor: 'message',
                        flexDirection: 'column',
                    }}
                >
                    <Flex sx={{ flexDirection: 'row' }}>
                        <Input
                            {...register('content', { required: true })}
                            px={3}
                            sx={{
                                outline: 0,
                                border: 0,
                            }}
                            placeholder="Nhắn tin..."
                            autoComplete="off"
                            onFocus={onSeen}
                        />
                        <Button
                            type="submit"
                            variant="transparent"
                            sx={{
                                flexShrink: 0,
                                borderTopRightRadius: 16,
                                borderBottomRightRadius: 16,
                            }}
                        >
                            Gởi
                        </Button>
                    </Flex>
                    {replyMessage && (
                        <Flex sx={{ flexDirection: 'row' }}>
                            <Flex
                                px={3}
                                py={2}
                                sx={{
                                    top: -19,
                                    left: 0,
                                    right: 0,
                                    flexDirection: 'column',
                                }}
                            >
                                <Text
                                    color="textSecondary"
                                    sx={{ fontSize: 0 }}
                                >
                                    Đang trả lời{' '}
                                    {userId === replyMessage.sender._id
                                        ? 'chính bạn'
                                        : `${replyMessage.sender.firstName}`}
                                </Text>
                                <Text color="text" sx={{ fontSize: 2 }}>
                                    <Linkify target="_blank">
                                        {ReactEmoji.emojify(
                                            replyMessage.content
                                        )}
                                    </Linkify>
                                </Text>
                            </Flex>
                            <Close
                                onClick={() => setReplyMessage(null)}
                                ml="auto"
                                sx={{
                                    svg: {
                                        width: 15,
                                        height: 15,
                                    },
                                    cursor: 'pointer',
                                }}
                            />
                        </Flex>
                    )}
                </Flex>
            </Flex>
        </>
    )
}

export default memo(ConversationPage)
