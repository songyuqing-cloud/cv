import { Flex, Text } from 'theme-ui'
import React, { FC, memo } from 'react'
import { gql, useQuery } from '@apollo/client'

export const CURRENT_USER = gql`
    query GetUser {
        currentUser {
            _id
            email
            firstName
            lastName
            phoneNumber
        }
    }
`

export interface CurrentUser {
    _id: string
    email: string
    firstName: string
    lastName: string
    phoneNumber: string
}

const Home: FC = () => {
    const { data } = useQuery<{ currentUser: CurrentUser }>(CURRENT_USER, {
        errorPolicy: 'all',
        fetchPolicy: 'network-only',
    })
    return (
        <Flex sx={{ flexDirection: 'column' }}>
            <Text>{data?.currentUser?.email}</Text>
            <Text>{data?.currentUser?.firstName}</Text>
            <Text>{data?.currentUser?.lastName}</Text>
            <Text>{data?.currentUser?.phoneNumber}</Text>
        </Flex>
    )
}

export default memo(Home)
