import React, { FC, memo, useEffect } from 'react'
import Link from 'next/link'
import { Flex, Button, Box, Field, Text, Message } from 'theme-ui'
import { useForm, SubmitHandler } from 'react-hook-form'
import { useRouter } from 'next/router'
import { useMutation } from '@apollo/client'
import useLocalStorage from '../hooks/local-storage'
import { LOGIN } from '../gql'

interface IFormInput {
    email: string
    password: string
}

const LoginPage: FC = () => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IFormInput>()
    const [login, { data, loading, error }] = useMutation<
        { signIn: string } | undefined
    >(LOGIN, {
        errorPolicy: 'all',
        fetchPolicy: 'no-cache',
    })
    const onSubmit: SubmitHandler<IFormInput> = (params) =>
        login({ variables: params })
    const { storedValue: authToken, setValue: setAuthToken } = useLocalStorage(
        'auth-token',
        ''
    )
    const router = useRouter()
    useEffect(() => {
        if (data?.signIn) {
            setAuthToken(data.signIn)
        }
    }, [data?.signIn, setAuthToken])
    useEffect(() => {
        if (authToken) {
            router.push('/')
        }
    }, [authToken, router])
    return (
        <>
            <Flex
                as="form"
                onSubmit={handleSubmit(onSubmit)}
                sx={{ flexDirection: 'column' }}
            >
                <Box mb={3}>
                    <Field
                        label="Tên đăng nhập"
                        {...register('email', {
                            required: 'Vui lòng nhập tên đăng nhập',
                        })}
                    />
                    {errors.email && (
                        <Text color="red" sx={{ fontSize: 1 }}>
                            {errors.email.message}
                        </Text>
                    )}
                </Box>
                <Box mb={3}>
                    <Field
                        label="Mật khẩu"
                        type="password"
                        {...register('password', {
                            required: 'Vui lòng nhập mật khẩu',
                        })}
                    />
                    {errors.password && (
                        <Text color="red" sx={{ fontSize: 1 }}>
                            {errors.password.message}
                        </Text>
                    )}
                </Box>
                {error && (
                    <Message variant="error" mb={3}>
                        {error.graphQLErrors
                            .map(({ message }) => message)
                            .join(', ')}
                    </Message>
                )}
                <Box ml="auto">
                    <Button mr={2} type="submit" disabled={loading}>
                        Đăng nhập
                    </Button>
                    <Link href="/sign-up" passHref>
                        <Button as="a" variant="secondary">
                            Đăng ký
                        </Button>
                    </Link>
                </Box>
            </Flex>
        </>
    )
}

export default memo(LoginPage)
