import { Theme } from 'theme-ui'

export const theme: Theme = {
    breakpoints: ['40em', '52em', '64em'],
    fontSizes: [12, 14, 16, 20, 24, 32, 48, 64],
    space: [0, 4, 8, 16, 32, 64, 128, 256],
    radii: [4, 18, 30, 40],
    colors: {
        text: '#040405',
        black: '#121212',
        textSecondary: 'rgba(4, 4, 5, 0.5)',
        background: '#fff',
        primary: '#54ca95',
        secondary: '#d5d5d5',
        borderColor: 'rgba(4, 4, 5, 0.1)',
        borderHoverColor: 'rgba(4, 4, 5, 0.2)',
        message: 'rgb(239,239,239)',
        modes: {
            dark: {
                text: '#fff',
                textSecondary: 'rgba(255, 255, 255, 0.5)',
                background: '#121212',
                borderColor: 'rgba(255, 255, 255, 0.1)',
                borderHoverColor: 'rgba(255, 255, 255, 0.2)',
            },
        },
    },
    fontWeights: {
        body: 500,
        semiBold: 600,
        bold: 700,
        heavy: 900,
    },
    lineHeights: {
        body: 1.5,
        heading: 1.25,
    },
    shadows: {
        small: '0 0 4px rgba(0, 0, 0, .3)',
        large: '0 0 24px rgba(0, 0, 0, .3)',
    },
    text: {
        default: {
            color: 'text',
            fontSize: 3,
        },
        heading: {
            fontFamily: 'heading',
            fontWeight: 'bold',
            lineHeight: 'heading',
        },
    },
    buttons: {
        primary: {
            py: 2,
            px: 3,
            fontSize: 1,
            lineHeight: 1.2,
            outline: 0,
            borderWidth: 2,
            borderStyle: 'solid',
            borderColor: 'primary',
            color: 'text',
            backgroundColor: 'transparent',
            borderRadius: 0,
            transition: 'all 0.3s ease-in-out',
            boxShadow: '0 10px 10px -8px rgba(0, 0, 0, 0.22)',
            cursor: 'pointer',
            ':hover': {
                color: '#fff',
                borderColor: 'primary',
                backgroundColor: 'primary',
            },
            ':disabled': {
                opacity: 0.5,
            },
        },
        secondary: {
            py: 2,
            px: 3,
            fontSize: 1,
            lineHeight: 1.2,
            outline: 0,
            borderWidth: 2,
            borderStyle: 'solid',
            borderColor: 'secondary',
            color: 'text',
            backgroundColor: 'transparent',
            borderRadius: 0,
            transition: 'all 0.3s ease-in-out',
            boxShadow: '0 10px 10px -8px rgba(0, 0, 0, 0.22)',
            cursor: 'pointer',

            ':hover': {
                borderColor: 'secondary',
                backgroundColor: 'secondary',
            },
            ':disabled': {
                opacity: 0.5,
            },
        },
        transparent: {
            py: 2,
            px: 3,
            color: 'primary',
            fontSize: 1,
            lineHeight: 1.2,
            outline: 0,
            bg: 'transparent',
            cursor: 'pointer',
            borderRadius: 1,
        },
    },
    messages: {
        error: {
            borderLeftColor: 'red',
            bg: 'rgb(255, 0, 0 , 0.5)',
            color: 'white',
        },
    },
}
