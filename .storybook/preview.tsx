import '../src/styles/globals.css'
import React from 'react'
import { Box, ThemeProvider, useColorMode, Switch } from 'theme-ui'
import { theme } from '../src/styles/theme'

const ChangeMode = () => {
    const [colorMode, setColorMode] = useColorMode()
    return (
        <Switch
            onClick={() =>
                setColorMode(colorMode === 'default' ? 'dark' : 'default')
            }
            label={colorMode === 'default' ? 'Dark' : 'Light'}
        />
    )
}

export const decorators = [
    (Story: React.FC<{}>) => (
        <ThemeProvider theme={theme}>
            <ChangeMode />
            <Box sx={{ width: '100%' }}>
                <Story />
            </Box>
        </ThemeProvider>
    ),
]
